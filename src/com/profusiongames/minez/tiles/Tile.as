package com.profusiongames.minez.tiles 
{
	import com.profusiongames.minez.states.Game;
	import flash.display.Shape;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.filters.GlowFilter;
	import flash.geom.Rectangle;
	import flash.text.TextField;
	import flash.text.TextFormat;
	
	/**
	 * ...
	 * @author UG
	 */
	public class Tile extends Sprite 
	{
		public var left:Tile;
		public var right:Tile;
		public var up:Tile;
		public var down:Tile;
		
		private var _bounds:Rectangle;
		
		private var _mineCount:int = 0;//mines adjacent to this tile
		private var _isShowing:Boolean = false;
		private var _isMine:Boolean = false;
		private var _isFlagged:Boolean = false;
		private var _isUnsure:Boolean = false;
		
		private var _background:Shape = new Shape();
		private var _flag:Shape = new Shape();
		private var _unsure:Shape = new Shape();
		
		private var _field:TextField = new TextField();
		private var _dim:int = 0;
		private var _isSelected:Boolean;
		public function Tile(dim:int) 
		{
			_dim = dim;
			addChild(_background);
			_flag.graphics.beginFill(0xFF0000);
			_flag.graphics.drawCircle(_dim / 2, _dim / 2, _dim / 3);
			_flag.graphics.endFill();
			
			_unsure.graphics.beginFill(0x111111);
			_unsure.graphics.drawRect(_dim / 2 - _dim / 6, _dim / 2 - _dim / 6, _dim / 3, _dim / 3);
			_unsure.graphics.endFill();
			
			_field.defaultTextFormat = new TextFormat("Arial", 18, 0x212121, true, null, null, null, null, 'center');
			_field.selectable = false;
			_field.width = _dim;
			_field.height = _dim;
			addChild(_field);
			addEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		private function init(e:Event):void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
			
			
			
			
			mineCount = 0;
			isMine = false;
			isShowing = false;
			
			addEventListener(MouseEvent.CLICK, onClick, false, -9999);
			addEventListener(MouseEvent.RIGHT_CLICK, onRightClick);
		}
		
		private function onClick(e:MouseEvent):void 
		{
			//check if mine
				//blowup
			//display adj mine count
			
			if (!Game.i.timer.running)
				Game.i.timer.start();
			trace("on click");
			
			if (Game.i.powerupClickInterupt)
			{
				Game.i.onClickedTileForPowerup(this);
				return;
			}
			
			if (isFlagged)
				return;
			if (isUnsure)
				isUnsure = false;
			if (isMine)
			{
				//blowup
				isShowing = true;
				Game.i.loseGame();
			}
			else
			{
				isShowing = true;
				Game.i.points++;
			}
			
			Game.i.checkForWin();
		}
		
		private function onRightClick(e:MouseEvent):void 
		{
			//if not flagged, flag
			//else if flagged
				//unsure
			//if unsure
				//normal
				
			if (isShowing)
				return;
				
			
			if (!isFlagged && !isUnsure)
			{
				isUnsure = false;
				isFlagged = true;
			}
			else if (isFlagged && !isUnsure)
			{
				isUnsure = true;
				isFlagged = false;
			}
			else if (!isFlagged && isUnsure)
			{
				isUnsure = false;
				isFlagged = false;
			}
			Game.i.checkForWin();
		}
		
		public function get isMine():Boolean 
		{
			return _isMine;
		}
		
		public function set isMine(value:Boolean):void 
		{
			_isMine = value;
		}
		
		public function get mineCount():int 
		{
			return _mineCount;
		}
		
		public function set mineCount(value:int):void 
		{
			_mineCount = value;
			var c:uint = 0;
			switch(value) {
				case 1:
					c = 0x73B1B7;
					break;
				case 2:
					c = 0x2E8B57;
					break; 
				case 3:
					c = 0xB22222;
					break;
				case 4:
					c = 0x104E8B;
					break;
				case 5:
					c = 0x8B1A1A;
					break;
				case 6:
					c = 0x00C78C;
					break;
				case 7:
					c = 0xCD0000;
					break;
				case 8:
					c = 0x8B0000;
					break;
			}
			
			_field.textColor = c;
			if (value == 0 || isMine)
				_field.text = "";
			else
				_field.text = "" + value;
		}
		
		public function get isShowing():Boolean 
		{
			return _isShowing;
		}
		
		public function set isShowing(value:Boolean):void 
		{
			_isShowing = value;
			_background.graphics.clear();
			if (value)
			{
				_field.visible = true;
				if (isMine)
				{
					_background.graphics.beginFill(0xAA0000, 0.2);
					_background.graphics.drawRect(0, 0, _dim, _dim);
					_background.graphics.endFill();
				}
				else
				{
					
					_background.graphics.beginFill(0xCCCCCC, 0.2);
					_background.graphics.lineStyle(1, 0xFFFFFF, 0.6);
					_background.graphics.drawRect(0, 0, _dim, _dim);
					_background.graphics.endFill();
					if (mineCount == 0)
					{
						floodFillStart(this, 0);
					}
				}
			}
			else
			{
				_field.visible = false;
				_background.graphics.beginFill(0xCCCCCC, 0.9);
				_background.graphics.lineStyle(1, 0x666666, 0.6);
				_background.graphics.drawRect(0, 0, _dim, _dim);
				_background.graphics.endFill();
			}
			
			_bounds = getRect(stage);
		}
		
		private function floodFillStart(t:Tile, count:int):void
		{
			floodFillAdjMines(t.left, count);
			floodFillAdjMines(t.right, count)
			floodFillAdjMines(t.down, count)
			floodFillAdjMines(t.up, count)
			
			
			if (t.left != null)
				floodFillAdjMines(t.left.up, count);	
			else if (t.up != null)
				floodFillAdjMines(t.up.left, count);
			
			if (t.left != null)
				floodFillAdjMines(t.left.down, count);	
			else if (t.down != null)
				floodFillAdjMines(t.down.left, count);
			
			if (t.right != null)
				floodFillAdjMines(t.right.up, count);	
			else if (t.up != null)
				floodFillAdjMines(t.up.right, count);
			
			if (t.right != null)
				floodFillAdjMines(t.right.down, count);	
			else if (t.down != null)
				floodFillAdjMines(t.down.right, count);
			
			/*if (t.left != null)
			{
				floodFillAdjMines(t.left.up, count);
				floodFillAdjMines(t.left.down, count);	
			}
			if (t.up != null)
			{
				floodFillAdjMines(t.up.left, count);
				floodFillAdjMines(t.up.right, count);
			}
			if (t.down != null)
			{
				floodFillAdjMines(t.down.left, count);
				floodFillAdjMines(t.down.right, count);
			}
			if (t.right != null)
			{
				floodFillAdjMines(t.right.up, count);
				floodFillAdjMines(t.right.down, count);
			}*/
		}
		
		private function floodFillAdjMines(t:Tile, count:int):void 
		{
			if (t == null)
				return;
			if (!t.isShowing && !t.isMine)
			{
				t.isShowing = true;
				if (t.mineCount == count)
				{
					//trace("we are floodfilling");
					floodFillStart(t, count);
				}
			}
		}
		
		public function containsPoint(dx:int, dy:int):Boolean 
		{
			return _bounds.contains(dx, dy);
		}
		
		
		public function containedIn(rect:Rectangle):Boolean //sx:int, sy:int, dx:int, dy:int):Boolean
		{
			if (_bounds == null)
				_bounds = getRect(stage);
			
			return rect.containsRect(_bounds);
			
			
			
			if (dx < 0)
			{
				sx -= dx;
				dx *= -1;
			}
			if (dy < 0)
			{
				sy -= dy;
				dy *= -1;
			}
			
			/*if ((x > sx && x < sx + dx && y > sy && y < sy + dy) || (x + _dim > sx && x + _dim < sx + dx && y + _dim > sy && y + _dim < sy + dy))
			{
				return true;
			}
			return false;*/
		}
		
		public function set isSelected(value:Boolean):void
		{
			if (_isSelected == value)
				return;
				
			_isSelected = value;
			
			if (value)
				filters = [ new GlowFilter(0x0000CC)];
			else
				filters = [];
		}
		
		public function get isSelected():Boolean
		{
			return _isSelected;
		}
		
		public function get isFlagged():Boolean 
		{
			return _isFlagged;
		}
		
		public function set isFlagged(value:Boolean):void 
		{
			if (value == _isFlagged)
				return;
				
			_isFlagged = value;
			if (value && _flag.parent == null)
			{
				addChild(_flag);
				Game.i.mineCount--;
			}
			else if (!value && _flag.parent != null)
			{
				removeChild(_flag);
				Game.i.mineCount++;
			}
		}
		
		public function get isUnsure():Boolean 
		{
			return _isUnsure;
		}
		
		public function set isUnsure(value:Boolean):void 
		{
			_isUnsure = value;
			
			if (value && _unsure.parent == null)
				addChild(_unsure);
			else if (!value && _unsure.parent != null)
				removeChild(_unsure);
			
		}
		
		
	}

}