package com.profusiongames.minez.tiles 
{
	import flash.display.Sprite;
	import flash.events.Event;
	
	/**
	 * ...
	 * @author UG
	 */
	public class Grid extends Sprite 
	{
		private var _tiles:Vector.<Vector.<Tile>> = new Vector.<Vector.<Tile>>();
		private var dim:int;
		private var minesToPlace:int;
		public function Grid(size:int, mines:int) 
		{
			dim = size;
			minesToPlace = mines;
			addEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		private function init(e:Event):void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
			createGrid();
			linkGrid();
			fillGridWithMines();
			countAdjacentMines();
		}
		
		private function linkGrid():void 
		{
			for (var i:int = 0; i < _tiles.length; i++)
			{
				for (var j:int = 0; j < _tiles[i].length; j++)
				{
					var tile:Tile = _tiles[i][j];
					
					if (i == 0) tile.up = null;
					else tile.up = _tiles[i - 1][j];
					
					if (j == 0) tile.left = null;
					else tile.left = _tiles[i][j - 1];
					
					if (i == _tiles[i].length - 1) tile.down = null;
					else tile.down = _tiles[i + 1][j];
					
					if (j == _tiles.length -1) tile.right = null;
					else tile.right = _tiles[i][j + 1];
				}
			}
		}
		
		public function recountAdjacentMines(t:Tile):void
		{
			for (var i:int = 0; i < _tiles.length; i++)
			{
				for (var k:int = 0; k < _tiles[i].length; k++)
				{
					if (_tiles[i][k] == t)
					{
						for (var a:int = -1; a <= 1; a++)
						{
							for (var b:int = -1; b <= 1; b++)
							{
								var adjCount:int = 0;
								adjCount += getAdjMineCountHelper(a + i - 1, b + k - 1);
								adjCount += getAdjMineCountHelper(a + i - 1, b + k + 0);
								adjCount += getAdjMineCountHelper(a + i - 1, b + k + 1);
								adjCount += getAdjMineCountHelper(a + i    , b + k - 1);
								//adjCount += getAdjMineCountHelper(i    , k);
								adjCount += getAdjMineCountHelper(a + i    , b + k + 1);
								adjCount += getAdjMineCountHelper(a + i + 1, b + k - 1);
								adjCount += getAdjMineCountHelper(a + i + 1, b + k + 0);
								adjCount += getAdjMineCountHelper(a + i + 1, b + k + 1);
								if(a + i >= 0 && b + k >= 0 && a + i < _tiles.length && b + k < _tiles[0].length)
									_tiles[a+i][b+k].mineCount = adjCount;
							}
						}
					}
				}
			}
		}
		
		private function countAdjacentMines():void 
		{
			for (var i:int = 0; i < _tiles.length; i++)
			{
				for (var k:int = 0; k < _tiles[i].length; k++)
				{
					var adjCount:int = 0;
					adjCount += getAdjMineCountHelper(i - 1, k - 1);
					adjCount += getAdjMineCountHelper(i - 1, k);
					adjCount += getAdjMineCountHelper(i - 1, k + 1);
					adjCount += getAdjMineCountHelper(i    , k-1);
					//adjCount += getAdjMineCountHelper(i    , k);
					adjCount += getAdjMineCountHelper(i    , k + 1);
					adjCount += getAdjMineCountHelper(i + 1, k - 1);
					adjCount += getAdjMineCountHelper(i + 1, k);
					adjCount += getAdjMineCountHelper(i + 1, k + 1);
					_tiles[i][k].mineCount = adjCount;
				}	
			}
		}
		
		private function getAdjMineCountHelper(dx:int, dy:int):int
		{
			if (dx < 0 || dy < 0 || dx >= _tiles.length || dy >= _tiles[0].length) return 0;
			return _tiles[dx][dy].isMine ? 1 : 0;
		}
		
		private function fillGridWithMines():void 
		{
			while (minesToPlace != 0)
			{
				var dx:int = int(Math.random() * _tiles.length);
				var dy:int = int(Math.random() * _tiles[0].length);
				if (!_tiles[dx][dy].isMine)
				{
					_tiles[dx][dy].isMine = true;
					minesToPlace--;
				}
			}
		}
		
		private function createGrid():void 
		{
			var padding:int = 2;
			var tileDim:int = 30//stage.stageWidth - padding * 2) / (dim + padding);
			for (var i:int = 0; i < dim; i++)
			{
				_tiles.push(new Vector.<Tile>());
				for (var k:int = 0; k < dim; k++)
				{
					var t:Tile = new Tile(tileDim);
					t.x = i * t.width + i * padding;
					t.y = k * t.height + k * padding;
					addChild(t);
					_tiles[i].push(t);
				}
			}
		}
		
		public function get tiles():Vector.<Vector.<Tile>> 
		{
			return _tiles;
		}
		
		public function set tiles(value:Vector.<Vector.<Tile>>):void 
		{
			_tiles = value;
		}
		
	}

}