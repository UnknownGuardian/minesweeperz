package com.profusiongames.minez.ui 
{
	import flash.display.Sprite;
	import flash.events.Event;
	
	/**
	 * ...
	 * @author UG
	 */
	public class PowerUpHUD extends Sprite 
	{
		private var _radar:PowerUpBox;
		private var _disableMine:PowerUpBox;
		private var _testMine:PowerUpBox;
		public function PowerUpHUD() 
		{
			addEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		private function init(e:Event):void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
			graphics.beginFill(0xCCCCCC, 0.9);
			graphics.lineStyle(1, 0x999999, 0.6);
			graphics.drawRect(5, 0, Main.WIDTH - x - 10, Main.HEIGHT - y - 10);
			graphics.endFill();
			
			_radar = new PowerUpBox("Radar");
			_radar.x = width / 2 - 100;
			_radar.y = 10;
			addChild(_radar);
			
			_disableMine = new PowerUpBox("Disable Mine");
			_disableMine.x = width / 2 - 100;
			_disableMine.y = _radar.y + _radar.height + 30;
			addChild(_disableMine);
			
			_testMine = new PowerUpBox("Test Mine");
			_testMine.x = width / 2 - 100;
			_testMine.y = _disableMine.y + _disableMine.height + 30;
			addChild(_testMine);
		}
		
	}

}