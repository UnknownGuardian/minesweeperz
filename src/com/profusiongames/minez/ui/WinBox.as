package com.profusiongames.minez.ui 
{
	import flash.display.Sprite;
	import flash.text.TextField;
	import flash.text.TextFormat;
	/**
	 * ...
	 * @author UnknownGuardian
	 */
	public class WinBox extends Sprite
	{
		private var _pointsField:TextField;
		private var _bonusTimeField:TextField;
		private var _bonusPointsField:TextField;
		public function WinBox() 
		{
			graphics.beginFill(0x000000, 0.9);
			graphics.drawRect(0, 0, Main.WIDTH, Main.HEIGHT);
			graphics.endFill();
			
			_pointsField = new TextField();
			_pointsField.defaultTextFormat = new TextFormat("Arial", 48, 0xDDDDDD, true, null, null, null, null, 'center');
			_pointsField.width = 500;
			_pointsField.height = 50;
			_pointsField.x = Main.WIDTH / 2 - 250;
			_pointsField.y = Main.HEIGHT / 2 - 100 - 25;
			addChild(_pointsField);
			
			
			_bonusTimeField = new TextField();
			_bonusTimeField.defaultTextFormat = new TextFormat("Arial", 32, 0xDDDDDD, true, null, null, null, null, 'center');
			_bonusTimeField.width = 500;
			_bonusTimeField.height = 50;
			_bonusTimeField.x = Main.WIDTH / 2 - 250;
			_bonusTimeField.y = Main.HEIGHT / 2-25;
			addChild(_bonusTimeField);
			
			_bonusPointsField = new TextField();
			_bonusPointsField.defaultTextFormat = new TextFormat("Arial", 32, 0xDDDDDD, true, null, null, null, null, 'center');
			_bonusPointsField.width = 500;
			_bonusPointsField.height = 50;
			_bonusPointsField.x = Main.WIDTH / 2 - 250;
			_bonusPointsField.y = Main.HEIGHT / 2 + 100 - 25;
			addChild(_bonusPointsField);
		}
		
		public function set bonusPoints(value:String):void
		{
			_bonusPointsField.text = value;
		}
		public function set bonusTime(value:String):void
		{
			_bonusTimeField.text = value;
		}
		
		public function set points(value:String):void
		{
			_pointsField.text = value;
		}
	}

}