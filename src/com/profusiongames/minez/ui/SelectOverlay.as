package com.profusiongames.minez.ui 
{
	import com.greensock.easing.Cubic;
	import com.greensock.TweenLite;
	import com.profusiongames.minez.states.Game;
	import com.profusiongames.minez.tiles.Tile;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.geom.Rectangle;
	import flash.text.TextField;
	import flash.text.TextFormat;
	
	/**
	 * ...
	 * @author UG
	 */
	public class SelectOverlay extends Sprite 
	{
		private var _field:TextField;
		private var sX:int = 0;
		private var sY:int = 0;
		private var selectedRectangle:Rectangle;
		public function SelectOverlay() 
		{
			addEventListener(MouseEvent.MOUSE_DOWN, mDown);
			graphics.beginFill(0x000000, 0.3);
			graphics.drawRect(0, 0, Main.WIDTH, Main.HEIGHT);
			graphics.endFill();
			
			_field = new TextField();
			_field.defaultTextFormat = new TextFormat("Arial", 24, 0x212121, true, null, null, null, null, 'center');
		}
		
		private function mDown(e:MouseEvent):void 
		{
			sX = e.stageX;
			sY = e.stageY;
			
			if (sX < Game.i.grid.width+5 && sY >= Game.i.grid.y-5)
			{
				stage.addEventListener(MouseEvent.MOUSE_MOVE, onFrame);
				stage.addEventListener(MouseEvent.MOUSE_UP, mUp);
			}
		}
		
		private function mUp(e:MouseEvent):void 
		{
			Game.i.onSelectedTilesForPowerup();
			//graphics.clear();
			stage.removeEventListener(MouseEvent.MOUSE_MOVE, onFrame);
			stage.removeEventListener(MouseEvent.MOUSE_UP, mUp);
		}
		
		public function showText(text:String):void
		{
			_field.x = selectedRectangle.x;
			_field.y = selectedRectangle.y;
			_field.width = selectedRectangle.width;
			_field.height = selectedRectangle.height;
			_field.text = text;
			addChild(_field);
			
			TweenLite.to(this, 1, { delay:2, alpha:0, onComplete:kill, ease:Cubic.easeInOut } );
		}
		
		public function fadeOut():void 
		{
			TweenLite.to(this, 1, { alpha:0, onComplete:kill, ease:Cubic.easeInOut } );
		}
		
		private function kill():void 
		{
			if (_field.parent != null)
				removeChild(_field);
			graphics.clear();
			parent.removeChild(this);
		}
		
		private function onFrame(e:MouseEvent):void 
		{
			var eX:int = Math.min(e.stageX, Game.i.grid.width + 5);
			var eY:int = Math.max(e.stageY, Game.i.grid.y - 5);
			var dX:int = eX - sX;
			var dY:int = eY - sY;
			graphics.clear();
			graphics.beginFill(0x000000, .3);
			graphics.drawRect(sX, sY, dX, dY);
			graphics.endFill();
			trace(sX, sY, eX, eY);
			
			
			
			/*if (dX < 0)
			{
				sX -= dX;
				dX *= -1;
			}
			if (dY < 0)
			{
				sY -= dY;
				dY *= -1;
			}*/
			selectedRectangle = new Rectangle(sX, sY, dX, dY);
			
			var tiles:Vector.<Vector.<Tile>> = Game.i.grid.tiles;
			for (var i:int = 0; i < tiles.length; i++)
			{
				for (var k:int = 0; k < tiles[0].length; k++)
				{
					if (tiles[i][k].containedIn(selectedRectangle))
						tiles[i][k].isSelected = true;
					else
						tiles[i][k].isSelected = false;
				}
			}
		}
		
	}

}