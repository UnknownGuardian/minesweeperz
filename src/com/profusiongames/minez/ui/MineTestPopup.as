package com.profusiongames.minez.ui 
{
	import com.greensock.easing.Cubic;
	import com.greensock.TweenLite;
	import flash.display.Sprite;
	import flash.text.TextField;
	import flash.text.TextFormat;
	
	/**
	 * ...
	 * @author UnknownGuardian
	 */
	public class MineTestPopup extends Sprite 
	{
		private var _field:TextField;
		public function MineTestPopup() 
		{
			_field = new TextField();
			_field.defaultTextFormat = new TextFormat("Arial", 24, 0x212121, true, null, null, null, null, 'center');
		}
		
		
		public function showText(text:String):void
		{
			graphics.beginFill(0x000000, 0.3);
			graphics.drawRect(0, 0, Main.WIDTH, Main.HEIGHT);
			graphics.endFill();
			
			_field.y = Main.HEIGHT / 2 - _field.height / 2;
			_field.selectable = false;
			_field.width =  Main.WIDTH;
			_field.height = 100;
			_field.text = text;
			addChild(_field);
			
			TweenLite.to(this, 1, { delay:2, alpha:0, onComplete:kill, ease:Cubic.easeInOut } );
		}
		
		private function kill():void 
		{
			if (_field.parent != null)
				removeChild(_field);
			graphics.clear();
			parent.removeChild(this);
		}
		
	}

}