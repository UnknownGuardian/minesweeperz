package com.profusiongames.minez.ui 
{
	import com.profusiongames.minez.states.Game;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.text.TextField;
	import flash.text.TextFormat;
	
	/**
	 * ...
	 * @author UG
	 */
	public class PowerUpBox extends Sprite 
	{
		private var _field:TextField;
		private var _text:String;
		public function PowerUpBox(text:String) 
		{
			_text = text;
			_field = new TextField();
			_field.defaultTextFormat = new TextFormat("Arial", 24, 0x666666, true, null, null, null, null, 'center');
			_field.selectable = false;
			_field.width = 200;
			_field.height = 40;
			_field.x = 5;
			_field.y = 5;
			_field.text = text;
			addChild(_field);
			
			graphics.beginFill(0xEEEEEE, 1);
			graphics.lineStyle(1, 0x999999, 0.6);
			graphics.drawRect(0, 0, _field.width + 10, _field.height + 10);
			graphics.endFill();
			
			addEventListener(MouseEvent.CLICK, onClick);
		}
		
		private function onClick(e:MouseEvent):void 
		{
			e.stopImmediatePropagation();
			Game.i.doPowerUp(_text);
		}
		
	}

}