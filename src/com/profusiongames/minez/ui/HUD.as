package com.profusiongames.minez.ui 
{
	import flash.display.Sprite;
	import flash.text.TextField;
	import flash.text.TextFormat;
	
	/**
	 * ...
	 * @author UG
	 */
	public class HUD extends Sprite 
	{
		private var _minesLeftField:TextField;
		private var _timerField:TextField;
		private var _pointsField:TextField;
		public function HUD(mineCount:int, timeLeft:int) 
		{
			_minesLeftField = new TextField();
			_minesLeftField.defaultTextFormat = new TextFormat("Arial", 32, 0x212121, true, null, null, null, null, 'left');
			_minesLeftField.width = 300;
			_minesLeftField.height = 50;
			mines = mineCount;
			_minesLeftField.x = 10;
			_minesLeftField.y = 10;
			addChild(_minesLeftField);
			
			_pointsField = new TextField();
			_pointsField.defaultTextFormat = new TextFormat("Arial", 32, 0x212121, true, null, null, null, null, 'center');
			_pointsField.width = 300;
			_pointsField.height = 50;
			_pointsField.x = Main.WIDTH / 2 - 150;
			_pointsField.y = 10;
			addChild(_pointsField);
			
			_timerField = new TextField();
			_timerField.defaultTextFormat = new TextFormat("Arial", 32, 0x212121, true, null, null, null, null, 'right');
			_timerField.width = 300;
			_timerField.height = 50;
			timer = timeLeft;
			_timerField.x = Main.WIDTH - 300 - 10;
			_timerField.y = 10;
			addChild(_timerField);
			
			graphics.beginFill(0xCCCCCC, 0.9);
			graphics.lineStyle(1, 0x999999, 0.6);
			graphics.drawRect(5, 5, Main.WIDTH - 10, height +5);
			graphics.endFill();
		}
		
		public function set timer(value:int):void
		{
			_timerField.text = "Bonus Time: " + value;
		}
		
		public function set mines(value:int):void
		{
			_minesLeftField.text = "Mines Left: " + value;
		}
		
		public function set points(value:int):void
		{
			_pointsField.text = value + " points";
		}
		
	}

}