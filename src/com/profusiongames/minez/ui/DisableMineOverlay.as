package com.profusiongames.minez.ui 
{
	import com.greensock.easing.Cubic;
	import com.greensock.TweenLite;
	import com.profusiongames.minez.states.Game;
	import com.profusiongames.minez.tiles.Tile;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	/**
	 * ...
	 * @author UnknownGuardian
	 */
	public class DisableMineOverlay extends Sprite 
	{
		
		public function DisableMineOverlay() 
		{
			graphics.beginFill(0x000000, 0.3);
			graphics.drawRect(0, 0, Main.WIDTH, Main.HEIGHT);
			graphics.endFill();
			
			
			addEventListener(Event.ADDED_TO_STAGE, init);
			
			mouseEnabled = false;
			Game.i.powerupClickInterupt = true;
		}
		
		private function init(e:Event):void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
			//stage.addEventListener(MouseEvent.CLICK, onDisableMineClick, false, 9999)
			stage.addEventListener(MouseEvent.MOUSE_MOVE, onMove);
		}
		
		private function onDisableMineClick(e:MouseEvent):void 
		{
			e.stopImmediatePropagation();
			trace("clickedxxx " + e.target);
			if (e.target is Tile)
			{
				trace("target looks to be a tiles");
				if (!(e.target as Tile).isShowing)
				{
					trace("target is not showing");
					stage.removeEventListener(MouseEvent.CLICK, onDisableMineClick);
					stage.removeEventListener(MouseEvent.MOUSE_MOVE, onMove);
					Game.i.onClickedTileForPowerup(e.target as Tile);
				}
			}
		}
		
		public function fadeOut():void 
		{
			stage.removeEventListener(MouseEvent.MOUSE_MOVE, onMove);
			TweenLite.to(this, 1, { alpha:0, onComplete:kill, ease:Cubic.easeInOut } );
		}
		
		private function kill():void 
		{
			graphics.clear();
			parent.removeChild(this);
		}
		
		private function onMove(e:MouseEvent):void 
		{
			var tiles:Vector.<Vector.<Tile>> = Game.i.grid.tiles;
			for (var i:int = 0; i < tiles.length; i++)
			{
				for (var k:int = 0; k < tiles[0].length; k++)
				{
					if (tiles[i][k].containsPoint(e.stageX, e.stageY))
						tiles[i][k].isSelected = true;
					else
						tiles[i][k].isSelected = false;
				}
			}
		}
		
	}

}