package com.profusiongames.minez.states 
{
	import com.profusiongames.minez.tiles.Grid;
	import com.profusiongames.minez.tiles.Tile;
	import com.profusiongames.minez.ui.DisableMineOverlay;
	import com.profusiongames.minez.ui.HUD;
	import com.profusiongames.minez.ui.MineTestPopup;
	import com.profusiongames.minez.ui.PowerUpHUD;
	import com.profusiongames.minez.ui.SelectOverlay;
	import com.profusiongames.minez.ui.WinBox;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.events.TimerEvent;
	import flash.utils.Timer;
	/**
	 * ...
	 * @author UG
	 */
	public class Game extends Sprite
	{
		public var powerupClickInterupt:Boolean = false;
		
		private static var _i:Game;
		private var _grid:Grid;
		private var _hud:HUD;
		private var _powerUpHud:PowerUpHUD;
		private var _selectOverlay:SelectOverlay;
		private var _disableOverlay:DisableMineOverlay;
		
		private var _timer:Timer;
		private var _gridSize:int = 16;
		private var _mineCount:int = 0;
		private var _timeLeft:int = 0;
		private var _points:int = 100;
		
		public function Game() 
		{
			_i = this;
			addEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		private function init(e:Event):void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
			
			_gridSize = 16;
			_mineCount = 40;
			_timeLeft = 200;
			
			createTimer();
			createHud();
			createGrid();
			createPowerUpHud();
		}
		
		private function createTimer():void 
		{
			_timer = new Timer(1000, 0);
			_timer.addEventListener(TimerEvent.TIMER, onTimer);
		}
		
		private function createHud():void 
		{
			_hud = new HUD(_mineCount, _timeLeft);
			addChild(_hud);
			
			_hud.points = points;
		}
		
		private function createPowerUpHud():void 
		{
			_powerUpHud = new PowerUpHUD();
			_powerUpHud.x = _grid.x + _grid.width;
			_powerUpHud.y = _grid.y;
			addChild(_powerUpHud);
		}
		
		private function createGrid():void 
		{
			_grid = new Grid(_gridSize, _mineCount);
			_grid.x = 5;
			_grid.y = 65;
			addChild(_grid);
		}
		
		public function get timeLeft():int 
		{
			return _timeLeft;
		}
		
		public function set timeLeft(value:int):void 
		{
			if (_timeLeft == value)
				return;
				
			_timeLeft = value;
			_hud.timer = value;
		}
		
		public function get mineCount():int 
		{
			return _mineCount;
		}
		
		public function set mineCount(value:int):void 
		{
			if (_mineCount == value)
				return;
			
			_mineCount = value;
			_hud.mines = value;
		}
		
		public static function get i():Game 
		{
			return _i;
		}
		
		public static function set i(value:Game):void 
		{
			if (_i == null)
				_i = new Game();
			_i = value;
		}
		
		public function get timer():Timer 
		{
			return _timer;
		}
		
		public function set timer(value:Timer):void 
		{
			_timer = value;
		}
		
		public function get grid():Grid 
		{
			return _grid;
		}
		
		public function set grid(value:Grid):void 
		{
			_grid = value;
		}
		
		public function get points():int { return _points; }
		
		public function set points(value:int):void { 
			_points = value;
			_hud.points = value;
		}
		
		private function onTimer(e:TimerEvent):void 
		{
			timeLeft--;
			if (timeLeft == 0)
			{
				_timer.removeEventListener(TimerEvent.TIMER, onTimer);
			}
		}
		
		public function doPowerUp(text:String):void 
		{
			trace("do powerup");
			
			if (powerupClickInterupt)
				return;
			if (text.toLowerCase() == "radar")
			{
				_selectOverlay = new SelectOverlay();
				addChild(_selectOverlay);
				/*user select tiles.
				if tile region is greater than 4x4 (16 tiles)
					give total mines in area
				else if 16 or less tiles
					if mine count in region is > 3)
						give total mines in area
					else 
						mark mines 
				*/
				
			}
			else if (text.toLowerCase() == "disable mine")
			{
				/*
				  user clicks tile
				  if tile is mine
					reduce mine count and give points (just like normally click on empty tile
				  else
					nothing
				*/
					
				_disableOverlay = new DisableMineOverlay();
				addChild(_disableOverlay);
					
				//stage.addEventListener(MouseEvent.CLICK, onDisableMineClick,false,9999);
			}
			else if (text.toLowerCase() == "test mine")
			{
				/*
				 test all unsure tiles
				 count how many are mines out of how many are unsure
				 display text
			     */
				 testUnsure();
			}
		}
		
		private function testUnsure():void 
		{
			var tiles:Vector.<Vector.<Tile>> = Game.i.grid.tiles;
			var unsureThatAreMinesCount:int = 0;
			var unsureCount:int = 0;
			for (var i:int = 0; i < tiles.length; i++)
			{
				for (var k:int = 0; k < tiles[0].length; k++)
				{
					if (tiles[i][k].isUnsure)//.isSelected)
					{
						unsureCount++;
						if (tiles[i][k].isMine)
							unsureThatAreMinesCount++;
					}
				}
			}
			
			points -= unsureCount * 10; //10 points per tile
			
			var pop:MineTestPopup = new MineTestPopup();
			pop.showText(unsureThatAreMinesCount + " out of " + unsureCount + " unsure sectors are mines");
			addChild(pop);
		}
		
		public function onSelectedTilesForPowerup():void 
		{
			var tiles:Vector.<Vector.<Tile>> = Game.i.grid.tiles;
			var selected:Vector.<Tile> = new Vector.<Tile>();
			var minesInSelected:int = 0;
			for (var i:int = 0; i < tiles.length; i++)
			{
				for (var k:int = 0; k < tiles[0].length; k++)
				{
					if (tiles[i][k].isSelected)//.isSelected)
					{
						tiles[i][k].isSelected = false;
						selected.push(tiles[i][k]);
						if (tiles[i][k].isMine)
							minesInSelected++;
					}
				}
			}
			
			points -= selected.length * 3; //3 points per tile
			
			if (selected.length > 16 || minesInSelected > 3)
			{
				_selectOverlay.showText("There are " + minesInSelected + " here");
			}
			else
			{
				_selectOverlay.fadeOut();
				for (var j:int = 0; j < selected.length; j++)
				{
					if (selected[j].isMine)
						selected[j].isFlagged = true;
				}
			}
		}
		
		public function onClickedTileForPowerup(t:Tile):void
		{
			if (t.isShowing)
				return;
			
			points -= 50; //50 points per tile
			
			Game.i.powerupClickInterupt = false;
			trace("onClickedTileForPowerup" + t);
			t.isSelected = false;
			if (t.isMine)
			{
				trace("it is a mine");
				t.isMine = false;
				t.isShowing = true;
				t.mineCount = 0;
				_grid.recountAdjacentMines(t);
			}
			else
			{
				t.isShowing = true;
				t.mineCount = 0;
			}
			_disableOverlay.fadeOut();
		}
		
		public function checkForWin():void 
		{	
			var realMinePoints:int = 0;
			var tiles:Vector.<Vector.<Tile>> = Game.i.grid.tiles;
			for (var i:int = 0; i < tiles.length; i++)
			{
				for (var k:int = 0; k < tiles[0].length; k++)
				{
					var t:Tile = tiles[i][k];
					if (!t.isShowing)
					{
						if (t.isFlagged)
						{
							if (t.isMine)
							{
								if(timeLeft > 0)
									realMinePoints += 5;
								else
									realMinePoints += 3;
							}
						}
						else if (t.isUnsure)
						{
							
						}
						else
						{
							return;
						}
					}
				}
			}
			
			//win
			
			_timer.stop();
			
			points += timeLeft * 2;
			points += realMinePoints;
			
			var w:WinBox = new WinBox();
			w.bonusPoints = "Bonus Points: " + realMinePoints;
			w.bonusTime = "Bonus Time: " + timeLeft * 2;
			w.points = "Total Points: " + points;
			
			stage.addChild(w);
		}
		
		public function loseGame():void 
		{
			_timer.stop();
			
			var w:WinBox = new WinBox();
			w.bonusPoints = "Game Over :(";
			w.bonusTime = "Try not to die next time!";
			w.points = "";
			
			stage.addChild(w);
		}
		
	}

}