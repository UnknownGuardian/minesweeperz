package 
{
	import com.profusiongames.minez.states.Game;
	import flash.events.Event;
	import flash.display.Sprite;
	import flash.display.StageAlign;
	import flash.display.StageScaleMode;
	
	/**
	 * ...
	 * @author UG
	 */
	public class Main extends Sprite 
	{
		public static var WIDTH:int = 0;
		public static var HEIGHT:int = 0;
		public function Main():void 
		{
			stage.scaleMode = StageScaleMode.NO_SCALE;
			stage.align = StageAlign.TOP_LEFT;
			
			WIDTH = stage.stageWidth;
			HEIGHT = stage.stageHeight;
			
			
			addChild(new Game());
			
			// entry point
		}
		
	}
	
}